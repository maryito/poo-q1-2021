# Clase padre o base
class Persona:
  __nombre = ""
  genero = ""
  __edad = ""
  __interes = ""

  def __init__(self, nombre, genero, edad, interes):
    self.setNombre(nombre)
    self.setEdad(edad)
    self.setInteres(interes)
    self.genero = genero
  
  def informacion(self):
    print("Soy {0} y tengo {1}".format(self.getNombre(), self.__edad))
    print("Mis interes son: {0} ".format(self.getInteres()))
  
  # Metodo Abstracto o interfaces
  def nombre_clase(self):
    pass

  def getNombre(self):
    return self.__nombre

  def setNombre(self, nombre):
    self.__nombre = nombre
  
  def getEdad(self):
    return self.__edad

  def setEdad(self, edad):
    self.__edad = edad

  def getInteres(self):
    return self.__interes

  def setInteres(self, interes):
    self.__interes = interes

# clases Hijas o derivadas
class Profesor(Persona):
  __cursos = []

  def getCursos(self):
    return self.__cursos
  
  def setCursos(self, nuevo_curso):
    self.__cursos.append(nuevo_curso)
  
  def eliminarCurso(self):
    print("Metodo de Eliminar")

    print("/n/n Listado de Cursos \n")

    contador = 0
    for x in self.__cursos:
      print("{0} - {1}".format(contador, x))
      contador = contador + 1 
    
    opc = int(input("\nSeleccione el curso a Eliminar ===> "))

    print("Se eliminar el curso: {0}".format(self.__cursos[opc]))

    seleccionado = self.__cursos[opc]
    self.__cursos.remove(seleccionado)
    print(self.__cursos)
    input()

class Estudiante(Persona):
  listado_clases = []

  def nombre_clase(self):
    print("\n\n Metodo para Matricular cursos \n")
    status = False
    contador = 1 
    while (status != True):
      nom = input("Nombre del curso #{0}: ".format(contador))
      self.listado_clases.append(nom)

      opc = int(input("¿Deseas Agregar mas Curso Si=0 / No=1 ?"))
      if opc == 1:
        status = True
      else:
        contador = contador + 1

  def mostrar_clases(self):
    print(self.listado_clases)

## Instancia de profesor 
# maryon = Profesor("Maryon","M", "25", "Inteligencia Artificial")

# maryon.informacion()

# # Agregando
# maryon.setCursos("Lenguaje de Programacion Orienta a Objeto")
# maryon.setCursos("Sistemas Operativos ")
# maryon.setCursos("Logica de Pogramación")
# maryon.setCursos("Matetimatica")

# print(maryon.getCursos())

# maryon.eliminarCurso()

## Instancia de Estudiante

est = Estudiante("Juan","M","20", "Programacion") 
est.informacion()
est.nombre_clase()
est.mostrar_clases()

