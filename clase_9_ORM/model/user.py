from databases.conector import *
from sqlalchemy import Column, Integer, String

class Usuario(Base):
    __tablename__ = "usuarios"
    id = Column(Integer, primary_key=True, index=True)
    nombre = Column(String(30))
    apellido = Column(String(30))
    password = Column(String(16))
    edad = Column(Integer)
