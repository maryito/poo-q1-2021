# Libreria necesarioas
"""
ORM ====> sqlalchemy
libreia base datos ====> PyMySQL
"""
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# conectores de base datos

engine = create_engine('mysql+pymysql://root:bd123456@localhost/lpoo')

Base = declarative_base()

# Manejo de las sesiones
Sesion = sessionmaker(bind=engine)
session = Sesion()